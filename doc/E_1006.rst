E1006
=====

.. note::
  The figure(s) given herein exist without technical context and for the sole
  purpose of communicating how they are generated.  If one wishes to use the
  figure(s) (and generating processes) herein, he or she must consult the
  associated technical standard, described next.

This chapter describes how to produce the figures for ASTM E10.05 Standard Test
Method 1006.  The recommended citation for this standard is:

* ASTM E1006-13, Standard Practice for Analysis and Interpretation of Physics
  Dosimetry Results from Test Reactor Experiments, ASTM International, West
  Conshohocken, PA, 2013, www.astm.org

