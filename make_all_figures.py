#!/usr/bin/env python

################################################################################
# EXECUTE PROGRAM ##############################################################
################################################################################

import logging

import __main__ as main


def setup_logging(loglevel=logging.INFO):
    logging.addLevelName(
        logging.DEBUG, "\033[1;33m%s\033[1;0m" % logging.getLevelName(logging.DEBUG)
    )
    logging.addLevelName(
        logging.INFO, "\033[1;32m%s\033[1;0m" % logging.getLevelName(logging.INFO)
    )
    logging.addLevelName(
        logging.WARNING, "\033[1;31m%s\033[1;0m" % logging.getLevelName(logging.WARNING)
    )
    logging.addLevelName(
        logging.ERROR, "\033[1;41m%s\033[1;0m" % logging.getLevelName(logging.ERROR)
    )
    logging.basicConfig(
        format="%(asctime)s %(levelname)-8s: %(message)s",
        level=loglevel,
        datefmt="%Y-%m-%d %H:%M:%S",
    )
    return


if __name__ == "__main__" and hasattr(main, "__file__"):
    import argparse
    import os
    import sys
    import textwrap

    import scripts_generate_figures.E_0263 as E_0263
    import scripts_generate_figures.E_0264 as E_0264
    import scripts_generate_figures.E_0266 as E_0266
    import scripts_generate_figures.E_0481 as E_0481
    import scripts_generate_figures.E_0523 as E_0523
    import scripts_generate_figures.E_0666 as E_0666
    import scripts_generate_figures.E_0693 as E_0693
    import scripts_generate_figures.E_0704 as E_0704
    import scripts_generate_figures.E_0705 as E_0705
    import scripts_generate_figures.E_1297 as E_1297

    supported_standards = {
        "263": E_0263,
        "264": E_0264,
        "266": E_0266,
        "481": E_0481,
        "523": E_0523,
        "666": E_0666,
        "693": E_0693,
        "704": E_0704,
        "705": E_0705,
        "1297": E_1297,
    }

    supported_standards_listing = "\n" + textwrap.fill(
        ", ".join(
            ["{:}".format(k) for k in sorted(list(supported_standards.keys()), key=int)]
        ),
        width=80,
    )

    description = textwrap.dedent(
        """
This script is used to generate figures for ASTM International E10 standards.
In general, it is run by a GitLab Docker instance, which will produce
high-resolution PNG images and a series of Sphinx-based pages that describe how
the images are produced.
    """
    )

    epilog = (
        textwrap.dedent(
            """
Typical command line calls might look like:

> python """
            + os.path.basename(__file__)
            + """

to produce all standards or

> python """
            + os.path.basename(__file__)
            + """ -s 705

to produce only one specific standard. When selecting a specific standard,
valid entries are:
"""
        )
        + supported_standards_listing
        + "\u2063"
    )

    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=description,
        epilog=epilog,
    )

    # Optional named argument(s).
    parser.add_argument(
        "--verbosity",
        "-V",
        type=int,
        default="1",
        help="degree of verbosity, from 0-2 (default: 1)",
    )

    parser.add_argument(
        "--standard",
        "-s",
        type=str,
        default="all",
        help="which standard to produce (default: all)",
    )

    outdir_default = "./doc/figures_generated"
    parser.add_argument(
        "--outdir",
        "-o",
        type=str,
        default=outdir_default,
        help="which directory to send figures to (default: " + outdir_default + ")",
    )

    args = parser.parse_args()

    logger = logging.getLogger()
    if args.verbosity == 0:
        setup_logging(logging.WARNING)
    elif args.verbosity == 1:
        setup_logging(logging.INFO)
    elif args.verbosity == 2:
        setup_logging(logging.DEBUG)
    else:
        logging.error("Unknown verbosity level; keeping default level.")

    # Only perform matplotlib operations after argparse in case just help is
    # requested.
    import matplotlib as mpl

    mpl.use("agg")
    import matplotlib.font_manager

    # Print out list of font locations and cleanup font manager listing.
    # These actions are taken to workaround an issue that if Times New Roman is
    # selected, then the bold version is inappropriately loaded.  This works in
    # concert with the YAML file to delete the bold fonts explicitly.  This action
    # is taken because because of inconsistent behavior between the GitLab Docker
    # runner and local font cache updating.
    fonts = [
        f
        for f in matplotlib.font_manager.findSystemFonts(fontpaths=None, fontext="ttf")
    ]
    for f in sorted(fonts):
        logging.debug(f)

    # Print versions of environment components.
    logging.info("Python version {:}".format(sys.version))
    logging.info("Matplotlib version {:}".format(mpl.__version__))

    # Generate figures.
    if args.standard.lower() == "all":
        for k, v in supported_standards.items():
            v.make_plots(destination=outdir_default)
    else:
        supported_standards[args.standard].make_plots(destination=outdir_default)
