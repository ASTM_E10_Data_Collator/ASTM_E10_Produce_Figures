#!/usr/bin/env python
# coding: utf-8
"""This file generates CSV-like files that describe fission-yield data for ASTM
International standard E1005."""

import ENDFtk
import logging
import sys

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


REACTION_PRODUCTS = [
    [40095, 0, "Zr-95"],
    [41095, 0, "Nb-95"],
    [44103, 0, "Ru-103"],
    [44106, 0, "Ru-106"],
    [45106, 0, "Rh-106"],
    [55137, 0, "Cs-137"],
    [56137, 1, "Ba-137m"],
    [56140, 0, "Ba-140"],
    [57140, 0, "La-140"],
    [58144, 0, "Ce-144"],
]


def get_e1005_table_2_fissionable_isotopes(
    tape, fissionable_isotopes, reaction_products
):
    """This function retrieves the desired reaction-product yields from the
    desired fissionable isotopes from an ENDF-formatted fission-yield tape."""
    for k, v in fissionable_isotopes.items():
        for fyd in [459, 454]:
            isotope = tape.material(v["material"]).file(8).section(fyd).parse()
            logging.debug(
                f"{k}({fyd}): Found {len(isotope.yields)} energy groups: "
                + ", ".join([f"{i}" for i in isotope.incident_energies])
            )

    s = ""
    for k, v in fissionable_isotopes.items():
        logging.debug(f"Parsing {k}...")
        s += f'{k+" ":-<80s}\n'
        for fp in reaction_products:
            s += f"{fp[2]:>7s}"
            for fyd in [459, 454]:
                for e in v["energy_groups"]:
                    isotope = tape.material(v["material"]).file(8).section(fyd).parse()
                    fission_products = isotope.yields[
                        e
                    ].fission_product_identifiers.to_list()
                    fission_yields = isotope.yields[e].fission_yields.to_list()

                    idx = fission_products.index(fp[0]) + fp[1]
                    y = fission_yields[idx][:]
                    if y[0] > 0:
                        if fyd == 454:
                            s += f",{y[0]*100:.4e} ± {y[1]/y[0]*100:.1f} %"
                        elif fyd == 459:
                            s += f",{y[0]*100:.4f} ± {y[1]/y[0]*100:.1f} %"
                    else:
                        s += f""
                if len(v["energy_groups"]) == 1:
                    s += f","

            s += "\n"

    s += "\n"

    return s


def main():
    """Execute process for JEFF 3.1.1 and JEFF 3.3."""
    infilename = "JEFF311NFY.ASC.txt"
    logging.info(f"Processing {infilename}...")
    outfile = open("fissyield_jeff31.csv", "w")

    fissionable_isotopes = {
        "Th-232": {
            "material": 3486,
            "energy_groups": [0],
        },
        "U-235": {
            "material": 3542,
            "energy_groups": [1, 0],
        },
        "Np-237": {
            "material": 3564,
            "energy_groups": [1],
        },
        "U-238": {
            "material": 3546,
            "energy_groups": [0],
        },
        "Pu-239": {
            "material": 3586,
            "energy_groups": [1, 0],
        },
    }

    tape = ENDFtk.tree.Tape.from_file(infilename)
    outfile.write(
        get_e1005_table_2_fissionable_isotopes(
            tape, fissionable_isotopes, REACTION_PRODUCTS
        )
    )
    outfile.close()

    infilename = "JEFF33-nfy.asc.txt"
    logging.info(f"Processing {infilename}...")
    outfile = open("fissyield_jeff33.csv", "w")

    fissionable_isotopes = {
        "Th-232": {
            "material": 9040,
            "energy_groups": [0],
        },
        "U-235": {
            "material": 9228,
            "energy_groups": [1, 0],
        },
        "Np-237": {
            "material": 9346,
            "energy_groups": [1],
        },
        "U-238": {
            "material": 9237,
            "energy_groups": [0],
        },
        "Pu-239": {
            "material": 9437,
            "energy_groups": [1, 0],
        },
    }

    tape = ENDFtk.tree.Tape.from_file(infilename)
    outfile.write(
        get_e1005_table_2_fissionable_isotopes(
            tape, fissionable_isotopes, REACTION_PRODUCTS
        )
    )
    outfile.close()


main()
