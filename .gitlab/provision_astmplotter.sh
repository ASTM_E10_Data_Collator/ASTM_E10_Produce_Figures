#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections

apt-get update

apt-get -y --no-install-recommends install \
  fontconfig \
  python3 \
  python3-matplotlib \
  python3-numpy \
  python3-pip \
  python3-scipy \
  ttf-mscorefonts-installer \
  wget

# Install necessary fonts and cleanup misbehaving fonts. This is a workaround
# and will hopefully be fixed properly at some point.
# wget http://ftp.de.debian.org/debian/pool/contrib/m/msttcorefonts/ttf-mscorefonts-installer_3.6_all.deb -P ~/Downloads
# apt install -y ~/Downloads/ttf-mscorefonts-installer_3.6_all.deb
rm /usr/share/fonts/truetype/msttcorefonts/Times_New_Roman_Bold.ttf
rm /usr/share/fonts/truetype/msttcorefonts/Times_New_Roman_Bold_Italic.ttf
rm /usr/share/fonts/truetype/msttcorefonts/Times_New_Roman_Italic.ttf
fc-cache -f -v

apt -y autoremove
