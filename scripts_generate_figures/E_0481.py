#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import __main__ as main


class E_0481:
    def __init__(self, destination="./doc/figures_generated"):
        self.destination = destination
        return

    def plot_figure_01(self, figure_width=3.5):
        import scripts_utilities.plotter as plotter
        import numpy as np

        # These points were used to construct the fitting equation below.
        x_pts = np.array(
            [
                0,
                0.0127000254000508,
                0.0190500381000762,
                0.0254000508001016,
                0.0381000762001524,
                0.0508001016002032,
                0.063500127000254,
                0.0762001524003048,
                0.0889001778003556,
                0.101600203200406,
                0.114300228600457,
                0.127000254000508,
                0.190500381000762,
                0.254000508001016,
                0.3175006350,
            ]
        )
        y_pts = np.array(
            [
                1,
                0.993031166654788,
                0.989504383565736,
                0.986022970902522,
                0.979181979454697,
                0.972500076606254,
                0.965961767397148,
                0.959552290200565,
                0.953257582411187,
                0.947093803422459,
                0.941025125993943,
                0.935075076669337,
                0.906811880919344,
                0.880723673492451,
                0.856605194293046,
            ]
        )

        @np.vectorize
        def f(x):
            return -0.2458 * x**3.0 + 0.4239 * x**2.0 - 0.5613 * x + 1

        p = plotter.plotter(figure_width=figure_width)
        # p.plt.plot(x_pts, y_pts, marker = '.') # Diagnostic output of point-picked data.

        x_min = 0
        x_max = 0.35
        x = np.linspace(x_min, x_max, int(100 * (x_max - x_min)))

        p.plt.plot(x, f(x))
        p.plt.xlabel(r"$\mathrm{Radius}\times\Sigma_{\gamma}(E_{\mathrm{res}})$ ")
        p.plt.ylabel(
            "Resonance Self-shielding"
            + "\n"
            + r"Factor, $G'_{\mathrm{res}}(\Theta=0.2)$"
        )
        p.plt.xscale("linear")
        p.plt.yscale("linear")
        p.plt.xlim(left=x_min, right=x_max)
        p.plt.ylim(bottom=0.84, top=1.0)
        p.plt.yticks([y for y in np.arange(0.84, 1.0, 0.02)])
        p.ax.grid()
        p.plt.savefig(self.destination + "/E_0481_Figure_01.png")
        return


def make_plots(destination):
    e0481 = E_0481(destination)
    logging.info(" Generating E481, Figure 1")
    e0481.plot_figure_01()


if __name__ == "__main__" and hasattr(main, "__file__"):
    make_plots()
