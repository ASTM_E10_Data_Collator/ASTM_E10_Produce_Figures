Enh: Fe-54 ENDF/B-VI; NRT dpa; Ed = 40.0 eV; Robinson; Z/A modification; 640-grp; 293.6K, 20.45C; 2200 m/s; 0.0253 eV
 0 0 0
 5 0 0 0 1 0 0 0 0 1
-- Fe-54 NRT displacement dpa using ENDF/B-VI in SAND-II 640-grp
--  
-- icntrl(1)=5:  NRT 3-interval threshold treatment (using 0.8 factor)
-- icntrl(5)=1:  Modification of Ed, lattice atom mass and Z - placed in heatr card
--               use elemental iron, in amu (it is then converted to neutron mass units), for lattice atomic weight
-- icntrl(8)=0:  Use built-in Robinson partition function
-- icntrl(9)=0:  Full Robinson treatment of recoil particles
-- icntrl(10)=1: Report dpa - not damage energy
--  
-- Temperature broadened to 293.6K - room temperature, 2200 m/s Maxwellian distribution neutrons
-- SAND-II 640-group output
moder
 20 -21
reconr
 -21 -22
 'pendf tape for n + 26-Fe-54 from ...'/
 2625 2 0
 0.001 0.0 /
 'n + 26-Fe-54 from ENDF/B-VI'/
 'processed by NJOY-2016'/
 0/
broadr
 -21 -22 -23
 2625 1 0 0  0.0 /
 .001 6.5e+6/
 293.6
 0/
heatr
 -21 -23 -24 25 
 2625 6 0 0 0 2 40.0  0 55.854   26.0 /
 303 442 443 444 445 447/
groupr
 -21 -24 0 28
 2625 15 0 4 0 1 1 1/
 'groupr: 640-group SAND-II'
 293.6                                                      
 1.0e10
 0.1   0.0253   100000.  1400000.0 /
 3 301 'Kerma'/
 3 443 'Kinematic Kerma Limit'/   
 3 444 '444 NRT dpa'/
 3 1 'total xsec'/  
  0 /
  0 /
dtfr
 28 29 0 0 /
 0 0 0 /
 1 640 4 5 645 1 0 /
'dpa' /
 1 444 1 /
 0 /
 'fe54' 2625 1 293.6 /
 /
stop
