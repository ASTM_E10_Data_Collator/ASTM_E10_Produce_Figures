.. _e0693:

E693 --- Figure Generation
==========================

.. note::
  The figure(s) given herein exist without technical context and for the sole
  purpose of communicating how they are generated.  If one wishes to use the
  figure(s) (and generating processes) herein, he or she must consult the
  associated technical standard, described next.

This chapter describes how to produce the data and resulting figure for ASTM
E10.05 Standard Test Method 693.  The recommended citation for this standard is:

* ASTM E693-17, Standard Practice for Characterizing Neutron Exposures in Iron
  and Low Alloy Steels in Terms of Displacements Per Atom (DPA), ASTM
  International, West Conshohocken, PA, 2017, www.astm.org

For this standard, one figure is required.  The process to generate the data
behind this figure, and the figure itself, is described next.

NJOY Data Processing
--------------------

A modified version of the NJOY-2016 code is required to properly process iron
isotope data to calculate damage response on an isotope-wise basis, which is
then folded with natural isotopic abundances to construct an elemental response.

The modifications necessary are not described herein; however, they are
available on `GitHub <https://github.com/pjgriff/SNL-NJOY-2016>`_.

The ENDF data file (which must be renamed to ``tape20``) and NJOY input files
needed are captured as described and linked in the following table.

================================================================================================================  =========================================
Filename                                                                                                          Description
----------------------------------------------------------------------------------------------------------------  -----------------------------------------
`E_0693_ENDF6_Fe.tape158.txt <E_0693_ENDF6_Fe.tape158.txt>`_                                                      ENDF6-formatted iron data.
`E_0693_ENDFB_VI_640grp_NRT-dpa_Fe-54.NJOY2016.inp.txt <E_0693_ENDFB_VI_640grp_NRT-dpa_Fe-54.NJOY2016.inp.txt>`_  NJOY input file for :superscript:`54`\ Fe
`E_0693_ENDFB_VI_640grp_NRT-dpa_Fe-56.NJOY2016.inp.txt <E_0693_ENDFB_VI_640grp_NRT-dpa_Fe-56.NJOY2016.inp.txt>`_  NJOY input file for :superscript:`56`\ Fe
`E_0693_ENDFB_VI_640grp_NRT-dpa_Fe-57.NJOY2016.inp.txt <E_0693_ENDFB_VI_640grp_NRT-dpa_Fe-57.NJOY2016.inp.txt>`_  NJOY input file for :superscript:`57`\ Fe
`E_0693_ENDFB_VI_640grp_NRT-dpa_Fe-58.NJOY2016.inp.txt <E_0693_ENDFB_VI_640grp_NRT-dpa_Fe-58.NJOY2016.inp.txt>`_  NJOY input file for :superscript:`58`\ Fe
================================================================================================================  =========================================

The resulting damage response is given in `E_0693_Fig_01.csv <E_0693_Fig_01.csv>`_\ .

Figure 1
--------

The recommended caption for this figure is:

    "ENDF/B-VI-based Iron Displacement Cross Section"

The data required to generate the figure is available directly in the standard.
As such, the data is extracted, sorted, and plotted.  The precision is such that
six digits are displayed following the decimal point, which is sufficient to
reflect all precision shown in the standard (as of the 693-17 version).  The
upper energy is 20 MeV.

Resulting Figures
_________________

The resulting figures follow.

.. _fig::E_0693_Figure_01
.. figure:: figures_generated/E_0693_Figure_01.png
   :width: 3.5in

   Figure 1: ENDF/B-VI-based Iron Displacement Cross Section


.. Code Documentation
.. ------------------

.. To be added...

.. .. automodule:: scripts_generate_figures.E_0693
..    :members:

.. Indices and tables
.. -----------------

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

