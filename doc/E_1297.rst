E1297 --- Figure Generation
===========================

.. note::
  The figure(s) given herein exist without technical context and for the sole
  purpose of communicating how they are generated.  If one wishes to use the
  figure(s) (and generating processes) herein, he or she must consult the
  associated technical standard, described next.

This chapter describes how to produce the figures for ASTM E10.05 Standard Test
Method 1297.  The recommended citation for this standard is:

* ASTM E1297-08(2013), Standard Test Method for Measuring Fast-Neutron Reaction
  Rates by Radioactivation of Niobium, ASTM International, West Conshohocken,
  PA, 2008, www.astm.org

For this standard, three figures are required.  The process to generate these
figures is described next.

Figure 1
--------

The recommended caption for this figure is:

    "RRDF / IRDFF-II Cross Section Versus Energy for the
    :superscript:`93`\ Nb(n,n')\ :superscript:`93m`\ Nb Reaction"

To retrieve the data to generate this figure, the JANIS application
( https://www.oecd-nea.org/janis ) is used with the following process:

1. Launch JANIS.

2. Under `Databases`, double click `Incident neutron data` → `IRDFF-II` →
   `Cross sections`.

3. Under `Nuclide / Compound`, double click `41 - Niobium` → `Nb93`.

4. In the new plotting window that appears, select `MT = 4 : (z,n')` →
   `Cross section for Niobium 93 1st metastable state production` → `T` to
   display a data table of the desired values.

5. Select `File` → `Export` and save the file in the ``data`` subdirectory with
   the filename ``E_1297_Fig_01.csv``.  Note that the ``.csv`` file extension is
   a bit of a misnomer because the column separator is a semicolon.  For the
   settings that should be used, see :ref:`fig:Example JANIS save file dialog`

.. _fig:Example JANIS save file dialog:
.. figure:: figures/E_1297_Doc_Figure_01.png
   :width: 6.5in

   Example JANIS save file dialog.

.. note::
   There appears to be no uncertainty data for either the Nb-93 multigroup or
   pointwise data.  The OECD/NEA contact for JANIS (janisinfo@oecd-nea.org) has
   been alerted to this fact.

Figure 2
--------

The recommended caption for this figure is:

    "Energy-dependent Uncertainty (%) for the RRDF / IRDFF-II
    :superscript:`93`\ Nb(n,n')\ :superscript:`93m`\ Nb Cross Section"

.. note::
   It is recommended that the E1297 Fig. 2 caption be updated to the caption
   given previously.

To retrieve the data to generate this figure, the IRDFF website is queried
directly:

1. Launch a web browser and navigate to https://www-nds.iaea.org/IRDFF/.

2. Download the **IRDFF-II dosimetry cross sections in 4-column format (Energy
   in eV, cross section in barn, absol. uncert. in barn,rel. uncert. in %, compressed)**)

3. Unzip the downloaded file (IRDFF-II_TAB.zip).

4. Extract the `Nb-93(n,n')Nb-93m` data set into a standalone text file.  The
   text file should consist of the three columns **without** the header
   `Nb-93(n,n')Nb-93m`. The final column must be deleted (rel. uncert.).

5. Save the resulting 3-column text file to `E_1297_Fig_02.csv``.  Note that the
   ``.csv`` file extension is a bit of a misnomer because the column separator
   is a space.

Figure 3
--------

The recommended caption for this figure is:

    "Comparison of the EXFOR Experimental Data with the RRDF / IRDFF-II
    :superscript:`93`\ Nb(n,n')\ :superscript:`93m`\ Nb Cross Section"

To retrieve this data, see `chap:Retrieving EXFOR Data`.

1. Navigate to https://www-nds.iaea.org/exfor/exfor.htm in a web browser.

2. Enter `Target` and `Product` search parameters as "Nb-93" and "Nb-93-m",
   respectively.

3. Click `Submit`.

4. Select the `41-NB-93(N,INL)41-NB-93-M,,SIG` experimental data set for
   plotting.  Deselect `W.H.Taylor+` and `F.Hegedues`. Click the `Quick-plot`
   checkbox.  Click the `Retrieve` button.

5. Retrieve the corresponding ENDF/B-VII.1 data and plot it.

6. Retrieve the raw data in ZView data format by clicking the `plotted data`
   hyperlink. Save the resulting ASCII file in the ``data`` subdirectory with
   the filename ``E_1297_Fig_03.zvd``.

Resulting Figures
_________________

The resulting figures follow.

.. _fig::E_1297_Figure_01
.. figure:: figures_generated/E_1297_Figure_01.png
   :width: 3.5in

   Figure 1: RRDF / IRDFF-II Cross Section Versus Energy for the :superscript:`93`\ Nb(n,n')\ :superscript:`93m`\ Nb Reaction

.. _fig::E_1297_Figure_02
.. figure:: figures_generated/E_1297_Figure_02.png
   :width: 3.5in

   Figure 2: Energy-dependent Uncertainty (%) for the RRDF / IRDFF-II :superscript:`93`\ Nb(n,n')\ :superscript:`93m`\ Nb Cross Section

.. _fig::E_1297_Figure_03
.. figure:: figures_generated/E_1297_Figure_03.png
   :width: 3.5in

   Figure 3: Comparison of the EXFOR Experimental Data with the RRDF / IRDFF-II :superscript:`93`\ Nb(n,n')\ :superscript:`93m`\ Nb Cross Section

Code Documentation
------------------

To be added...

.. automodule:: scripts_generate_figures.E_1297
   :members:

.. Indices and tables
.. -----------------

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

