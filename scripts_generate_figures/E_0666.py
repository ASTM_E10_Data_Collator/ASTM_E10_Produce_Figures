#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import __main__ as main


class E_0666:
    def __init__(self, destination="./doc/figures_generated"):
        self.destination = destination
        return

    def plot_figure_01(self, figure_width=3.5):
        import scripts_utilities.plotter as plotter
        import numpy as np

        x1 = np.linspace(0, 0.16)
        x2 = np.linspace(0.16, 2)
        y1 = np.exp(-x1)
        y1c = -1.5 * (x1 - 0.16) ** 2 + np.exp(-0.16)
        y2 = np.exp(-x2)

        p = plotter.plotter(figure_width=figure_width)
        p.plt.plot(x1, y1, "k--")
        p.plt.plot(x1, y1c, "k-")
        p.plt.plot(x2, y2, "k-")

        arrowprops = dict(arrowstyle="-|>")

        p.plt.annotate(
            "0.85",
            xy=(0.2, 0.85),
            xytext=(0.4, 0.85),
            arrowprops=arrowprops,
            va="center",
        )

        p.plt.xlim(xmin=0.00, xmax=1.00)
        p.plt.ylim(ymin=0.00, ymax=1.00)

        p.plt.xlabel("Depth (Arb. Units)")
        p.plt.ylabel("Absorbed Dose\n(Arb. Units)")
        p.plt.savefig(self.destination + "/E_0666_Figure_01.png")
        return

    def plot_figure_02(self, figure_width=3.5):
        import scripts_utilities.plotter as plotter
        import numpy as np

        x1 = np.linspace(0.01, 10, 100000)

        y1 = np.piecewise(
            x1, [x1 < 0.1, (x1 > 0.1) & (x1 < 1), x1 >= 1], [0, lambda x1: 0.1 / x1, 0]
        )

        p = plotter.plotter(figure_width=figure_width)
        p.plt.step(x1, y1)
        p.plt.xlabel("Photon Energy")
        p.plt.ylabel(
            "Number of Photons"
            + "\n"
            + r"($\mathrm{cm}^{-2}\cdot\mathrm{keV}^{-1}\cdot\mathrm{J}^{-1}$)"
        )
        p.plt.xlim(xmin=0.01, xmax=10)
        p.plt.xscale("log")
        p.plt.xticks((0.01, 0.1, 1, 10), ("10 keV", "100 keV", "1 MeV", "10 MeV"))
        p.plt.ylim(ymin=0, ymax=1.0)
        p.plt.savefig(self.destination + "/E_0666_Figure_02.png")
        return

    def plot_figure_03(self, figure_width=3.5):
        import scripts_utilities.plotter as plotter
        import numpy as np

        x1 = np.linspace(0.01, 10, 100000)

        y1_0 = np.piecewise(
            x1, [x1 < 0.1, (x1 > 0.1) & (x1 < 1), x1 >= 1], [0, lambda x1: 100, 0]
        )

        p = plotter.plotter(figure_width=figure_width)
        p.plt.plot(x1, y1_0)
        p.plt.xlabel("Photon Energy")
        p.plt.ylabel(
            "Energy Fluence"
            + "\n"
            + r"($\mathrm{keV}\cdot\mathrm{cm}^{-2}\cdot\mathrm{keV}^{-1}\cdot\mathrm{J}^{-1}$)"
        )
        p.plt.xlim(xmin=0.01, xmax=10)
        p.plt.xscale("log")
        p.plt.xticks((0.01, 0.1, 1, 10), ("10 keV", "100 keV", "1 MeV", "10 MeV"))
        p.plt.ylim(ymin=0, ymax=120)
        p.plt.savefig(
            self.destination + "/E_0666_Figure_03.png"
        )  # , bbox_inches = 'tight' )
        return

    def plot_figure_04(self, figure_width=3.5):
        import hashlib
        import scripts_utilities.plotter as plotter
        import numpy as np

        infilename = "data/E_0666_Fig_04.csv"
        assert (
            hashlib.md5(open(infilename, "rb").read()).hexdigest()
            == "dc4e2b1dd6456a746d626d6b687a81df"
        )
        e_MeV, mu_rho, muen_rho = np.loadtxt(infilename, unpack=True)

        # From: https://stackoverflow.com/a/29359275
        from scipy import interpolate

        def log_interp1d(xx, yy, kind="linear"):
            logx = np.log10(xx)
            logy = np.log10(yy)
            lin_interp = interpolate.interp1d(logx, logy, kind=kind)
            return lambda zz: np.power(10.0, lin_interp(np.log10(zz)))

        muen_rho_f = log_interp1d(e_MeV, muen_rho, kind="linear")

        def check_interp_function():
            p = plotter.plotter(figure_width=figure_width)
            p.plt.loglog(e_MeV, muen_rho, "x", label=r"NIST $\mu_{en}/\rho$")
            x = np.linspace(1e-3, 2e1, int(1e5))
            p.plt.loglog(x, muen_rho_f(x), label="fit")
            p.plt.legend(loc="best")
            p.plt.show()
            return

        p = plotter.plotter(figure_width=figure_width)

        def psi_0(E):
            return 0.1 / E

        def psi_t(E):
            t = 5  # 5 g/cm^2
            return E * 1000 * psi_0(E) * np.exp(-muen_rho_f(E) * t)

        x1 = np.linspace(0.01, 10, 100000)

        y1_0 = np.piecewise(
            x1, [x1 < 0.1, (x1 > 0.1) & (x1 < 1), x1 >= 1], [0, lambda x1: 100, 0]
        )

        y1 = np.piecewise(
            x1, [x1 < 0.1, (x1 > 0.1) & (x1 < 1), x1 >= 1], [0, lambda x1: psi_t(x1), 0]
        )

        p.plt.plot(x1, y1_0, "--", color="#aaaaaa")
        p.plt.plot(x1, y1, "k-")

        p.plt.xlim((0.01, 10))
        p.plt.xscale("log")
        p.plt.xticks((0.01, 0.1, 1, 10), ("10 keV", "100 keV", "1 MeV", "10 MeV"))
        p.plt.ylim((0, 120))
        p.plt.xlabel("Photon Energy")
        p.plt.ylabel(
            "Energy Fluence"
            + "\n"
            + r"($\mathrm{keV}\cdot\mathrm{cm}^{-2}\cdot\mathrm{keV}^{-1}\cdot\mathrm{J}^{-1}$)"
        )

        p.plt.savefig(self.destination + "/E_0666_Figure_04.png")
        return


def make_plots(destination):
    e0666 = E_0666(destination)
    logging.info(" Generating E0666, Figure 1")
    e0666.plot_figure_01()
    logging.info(" Generating E0666, Figure 2")
    e0666.plot_figure_02()
    logging.info(" Generating E0666, Figure 3")
    e0666.plot_figure_03()
    logging.info(" Generating E0666, Figure 4")
    e0666.plot_figure_04()


if __name__ == "__main__" and hasattr(main, "__file__"):
    make_plots()
