#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import __main__ as main


class E_0704:
    def __init__(self, destination="./doc/figures_generated"):
        self.destination = destination
        return

    def plot_figure_01(self, figure_width=3.5):
        import hashlib
        import scripts_utilities.plotter as plotter
        from scripts_utilities.retrieve_zvd_data import xs_data_exp_eval

        infilename = "data/E_0704_Fig_01.zvd"
        assert (
            hashlib.md5(open(infilename, "rb").read()).hexdigest()
            == "db169108b11c5ef85deef39bc7d8d762"
        )
        data = xs_data_exp_eval(infilename)

        x_exp = data.exp.X
        x_err = data.exp.dX
        y_exp = data.exp.Y
        y_err = data.exp.dY
        x_eval = data.eval.X
        y_eval = data.eval.Y

        p = plotter.plotter(figure_width=figure_width)
        p.plt.plot(x_eval, y_eval, color="#000000", zorder=10, label="Eval. Data")
        p.plt.errorbar(
            x_exp,
            y_exp,
            xerr=x_err,
            yerr=y_err,
            fmt="s",
            markersize=2.0,
            label="Exp. Data",
        )
        p.plt.xlabel("Neutron Energy (MeV)")
        p.plt.ylabel("Cross Section (barns)")
        p.plt.xscale("log")
        p.plt.yscale("linear")
        p.plt.xlim(left=0.5, right=20)
        p.plt.ylim(bottom=0.0, top=1.5)
        p.plt.xticks([10**x for x in range(0, 2)])
        p.plt.yticks([0.25 * y for y in range(0, 7)])
        p.ax.grid()
        p.plt.legend(loc="best", fontsize=8)
        p.plt.savefig(self.destination + "/E_0704_Figure_01.png")
        return

    def plot_figure_02(self, figure_width=3.5):
        import hashlib
        import scripts_utilities.plotter as plotter
        import numpy as np

        infilename = "data/E_0704_Fig_02.csv"
        assert (
            hashlib.md5(open(infilename, "rb").read()).hexdigest()
            == "3cc1f9636c2994728619fb56218c413e"
        )
        data = np.genfromtxt(infilename, delimiter=";", skip_header=4)
        x = np.array(data[:, 0]) / 1e6
        y = 100 * np.array(data[:, 2] / data[:, 1])

        p = plotter.plotter(figure_width=figure_width)
        p.plt.step(x, y)
        p.plt.xlabel("Neutron Energy (MeV)")
        p.plt.ylabel("Standard Deviation (%)")
        p.plt.xscale("log")
        p.plt.xlim(left=0.5, right=20)
        p.plt.ylim(bottom=0, top=5)
        p.plt.xticks([10**x for x in range(0, 2)])
        p.ax.grid()
        p.plt.savefig(self.destination + "/E_0704_Figure_02.png")
        return


def make_plots(destination):
    e0704 = E_0704(destination)
    logging.info(" Generating E704, Figure 1")
    e0704.plot_figure_01()
    logging.info(" Generating E704, Figure 2")
    e0704.plot_figure_02()


if __name__ == "__main__" and hasattr(main, "__file__"):
    make_plots()
