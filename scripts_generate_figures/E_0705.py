#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import __main__ as main


class E_0705:
    def __init__(self, destination="./doc/figures_generated"):
        self.destination = destination
        return

    def plot_figure_01(self, figure_width=3.5):
        import hashlib
        import scripts_utilities.plotter as plotter
        from scripts_utilities.retrieve_zvd_data import xs_data_exp_eval

        infilename = "data/E_0705_Fig_01.zvd"
        assert (
            hashlib.md5(open(infilename, "rb").read()).hexdigest()
            == "24c7346b79f22013da63c4e01b73fb8d"
        )
        data = xs_data_exp_eval(infilename)

        x_exp = data.exp.X
        x_err = data.exp.dX
        y_exp = data.exp.Y
        y_err = data.exp.dY
        x_eval = data.eval.X
        y_eval = data.eval.Y

        p = plotter.plotter(figure_width=figure_width)
        p.plt.plot(x_eval, y_eval, color="#000000", zorder=10, label="Eval. Data")
        p.plt.errorbar(
            x_exp,
            y_exp,
            xerr=x_err,
            yerr=y_err,
            fmt="s",
            markersize=2.0,
            label="Exp. Data",
        )
        p.plt.xlabel("Neutron Energy (MeV)")
        p.plt.ylabel("Cross Section (barns)")
        p.plt.xscale("log")
        p.plt.yscale("log")
        p.plt.xlim(left=1e-10, right=20)
        p.plt.ylim(bottom=1e-4, top=1e2)
        p.plt.xticks([10**x for x in range(-10, 2)])
        p.ax.grid()
        p.plt.legend(loc="best", fontsize=8)
        p.plt.savefig(self.destination + "/E_0705_Figure_01.png")
        return

    def plot_figure_02(self, figure_width=3.5):
        import hashlib
        import scripts_utilities.plotter as plotter
        import numpy as np

        infilename = "data/E_0705_Fig_02.csv"
        assert (
            hashlib.md5(open(infilename, "rb").read()).hexdigest()
            == "5f18ede0c8600973e6fa03ec7d64be85"
        )
        data = np.genfromtxt(infilename, delimiter=";", skip_header=4)
        x = np.array(data[:, 0]) / 1e6
        y = 100 * np.array(data[:, 2] / data[:, 1])

        p = plotter.plotter(figure_width=figure_width)
        p.plt.step(x, y)
        p.plt.xlabel("Neutron Energy (MeV)")
        p.plt.ylabel("Standard Deviation (%)")
        p.plt.xscale("log")
        p.plt.xlim(left=1e-10, right=20)
        p.plt.ylim(bottom=0, top=30)
        p.plt.xticks([10**x for x in range(-10, 2)])
        p.ax.grid()
        p.plt.savefig(self.destination + "/E_0705_Figure_02.png")
        return


def make_plots(destination):
    e0705 = E_0705(destination)
    logging.info(" Generating E705, Figure 1")
    e0705.plot_figure_01()
    logging.info(" Generating E705, Figure 2")
    e0705.plot_figure_02()


if __name__ == "__main__" and hasattr(main, "__file__"):
    make_plots()
