#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import __main__ as main


class E_1297:
    def __init__(self, destination="./doc/figures_generated"):
        self.destination = destination
        return

    def plot_figure_01(self, figure_width=3.5):
        import hashlib
        import scripts_utilities.plotter as plotter
        import numpy as np

        infilename = "data/E_1297_Fig_01.csv"
        assert (
            hashlib.md5(open(infilename, "rb").read()).hexdigest()
            == "10f3ba0408b56ef31002db10649819a5"
        )
        data = np.genfromtxt(infilename, delimiter=";", skip_header=3)
        x = np.array(data[:, 0])
        y = np.array(data[:, 1])

        # Convert eV to MeV.
        x = x / 1e6

        p = plotter.plotter(figure_width=figure_width)
        p.plt.step(x, y, where="post")
        p.plt.xlabel("Neutron Energy (MeV)")
        p.plt.ylabel("Cross Section (barns)")
        p.plt.xscale("log")
        p.plt.xlim(xmin=1e-1, xmax=2e1)
        p.plt.ylim(ymin=0.00, ymax=0.30)
        # p.plt.xticks( [ 10**x for x in range( -10, 2 ) ] )
        p.ax.grid()
        p.plt.savefig(self.destination + "/E_1297_Figure_01.png")
        return

    def plot_figure_02(self, figure_width=3.5):
        import hashlib
        import scripts_utilities.plotter as plotter
        import numpy as np

        infilename = "data/E_1297_Fig_02.csv"
        assert (
            hashlib.md5(open(infilename, "rb").read()).hexdigest()
            == "365f90b059c0f52725bb9cbd357fecc0"
        )
        data = np.genfromtxt("data/E_1297_Fig_02.csv", delimiter=" ")
        x = np.array(data[1:, 0])
        y = 100 * np.array(data[1:, 2] / data[1:, 1])

        # Convert eV to MeV.
        x = x / 1e6

        p = plotter.plotter(figure_width=figure_width)
        p.plt.step(x, y, where="post")
        p.plt.xlabel("Neutron Energy (MeV)")
        p.plt.ylabel("Standard Deviation (%)")
        p.plt.xlim(xmin=0, xmax=2e1)
        p.plt.ylim(ymin=2, ymax=16)
        # p.plt.xticks( [ 10**x for x in range( -10, 2 ) ] )
        p.ax.grid()
        p.plt.savefig(self.destination + "/E_1297_Figure_02.png")
        return

    def plot_figure_03(self, figure_width=3.5):
        import hashlib
        import scripts_utilities.plotter as plotter
        from scripts_utilities.retrieve_zvd_data import xs_data_exp
        import numpy as np

        # data = xs_data_exp_eval( 'data/E_1297_Fig_03.zvd' )
        infilename = "data/E_1297_Fig_03.zvd"
        assert (
            hashlib.md5(open(infilename, "rb").read()).hexdigest()
            == "9e1bc235bd79688eb12f0371e9ca5772"
        )
        data = xs_data_exp(infilename)

        x_exp = data.X
        x_err = data.dX
        y_exp = data.Y
        y_err = data.dY

        infilename = "data/E_1297_Fig_02.csv"
        assert (
            hashlib.md5(open(infilename, "rb").read()).hexdigest()
            == "365f90b059c0f52725bb9cbd357fecc0"
        )
        data = np.genfromtxt("data/E_1297_Fig_02.csv", delimiter=" ")
        x = np.array(data[1:, 0])
        y = np.array(data[1:, 1])

        # Convert eV to MeV.
        x = x / 1e6

        p = plotter.plotter(figure_width=figure_width)
        p.plt.plot(x, y, color="#000000", zorder=10, label="Eval. Data")
        p.plt.errorbar(
            x_exp,
            y_exp,
            xerr=x_err,
            yerr=y_err,
            fmt="s",
            markersize=2.0,
            label="Exp. Data",
        )
        p.plt.xlabel("Neutron Energy (MeV)")
        p.plt.ylabel("Cross Section (barns)")
        p.plt.xlim(xmin=0, xmax=20)
        p.plt.ylim(ymin=0, ymax=0.3)
        p.ax.grid()
        p.plt.legend(loc="best")
        p.plt.savefig(
            self.destination + "/E_1297_Figure_03.png"
        )  # , bbox_inches = 'tight' )
        return


def make_plots(destination):
    e1297 = E_1297(destination)
    logging.info(" Generating E1297, Figure 1")
    e1297.plot_figure_01()
    logging.info(" Generating E1297, Figure 2")
    e1297.plot_figure_02()
    logging.info(" Generating E1297, Figure 3")
    e1297.plot_figure_03()


if __name__ == "__main__" and hasattr(main, "__file__"):
    make_plots()
