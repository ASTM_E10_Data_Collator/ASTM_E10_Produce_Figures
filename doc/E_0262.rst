E262
====

.. note::
  The figure(s) given herein exist without technical context and for the sole
  purpose of communicating how they are generated.  If one wishes to use the
  figure(s) (and generating processes) herein, he or she must consult the
  associated technical standard, described next.

This chapter describes how to produce the figures for ASTM E10.05 Standard Test
Method 262.  The recommended citation for this standard is:

* ASTM E262-17, Standard Test Method for Determining Thermal Neutron Reaction
  Rates and Thermal Neutron Fluence Rates by Radioactivation Techniques, ASTM
  International, West Conshohocken, PA, 2017, www.astm.org

