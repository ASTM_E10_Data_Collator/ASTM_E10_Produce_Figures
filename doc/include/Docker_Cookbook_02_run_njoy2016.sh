#!/bin/bash

cd data
ln -sf E_0693_ENDF6_Fe.tape158.txt tape20
for isotope in Fe-54 Fe-56 Fe-57 Fe-58; do # Run NJOY for each iron isotope.
  export prefix="E_0693_ENDFB_VI_640grp_NRT-dpa_${isotope}"
  njoy < ${prefix}.NJOY2016.inp.txt > ${prefix}.stdout
  mv output ${prefix}.out && mv tape29 ${prefix}.tape29.txt
  rm -f *.tape2[1-8]
done
exit

