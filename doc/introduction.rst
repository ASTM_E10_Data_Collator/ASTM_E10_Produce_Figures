Introduction
============

The purpose of this document is to describe the tools and processes used to
generate the figures in the ASTM standards governed by ASTM committee E10
(Nuclear Technology and Applications).  This objective is a result of
subcommittee member turnover whereby it is not always immediately clear where
the technical data for a given figure is obtained from and/or how the data must
be processed to be maximally useful in an ASTM standard.

Future Work
-----------

In its current form, this document focuses on nuclear data figures.

However, it can be imagined that the production of all figures can be governed
by the tools that accompany this document.  For example, vector graphics can be
stored in the accompanying repository to be reliably re-used and/or drawing
commands can be kept to reliably regenerate figures, as needed.

In addition, the source of data in the standards' various tables, and how data
is evaluated for inclusion, is another topic that can be discussed in this
document in the future.
