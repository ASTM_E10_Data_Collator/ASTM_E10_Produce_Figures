#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import __main__ as main


class E_0263:
    def __init__(self, destination="./doc/figures_generated"):
        self.destination = destination
        return

    def plot_figure_01(self, figure_width=3.5):
        import hashlib
        import scripts_utilities.plotter as plotter
        import numpy as np

        infilename = "data/E_0263_Fig_01.csv"
        assert (
            hashlib.md5(open(infilename, "rb").read()).hexdigest()
            == "1ee4edf3efb2b89e1a669461cf62cf11"
        )
        data = np.genfromtxt(infilename, delimiter=";", skip_header=3)
        x = 1e-6 * np.array(data[:, 0])
        y = np.array(data[:, 1])

        p = plotter.plotter(figure_width=figure_width)
        p.plt.plot(x, y, label="$^{54}$Fe(n,p)$^{54}$Mn")
        p.plt.xlabel("Neutron Energy (MeV)")
        p.plt.ylabel("Cross Section (barns)")
        p.plt.xscale("linear")
        p.plt.yscale("linear")
        # p.plt.legend( loc = (0.25,0.25))
        p.plt.xlim(left=0, right=20)
        p.plt.ylim(bottom=0, top=0.6)
        p.plt.xticks([5 * x for x in range(1, 5)])
        p.plt.yticks([0.1 * y for y in range(0, 7)])
        p.ax.grid()
        p.plt.savefig(self.destination + "/E_0263_Figure_01.png")
        return


def make_plots(destination):
    e0263 = E_0263(destination)
    logging.info(" Generating E263, Figure 1")
    e0263.plot_figure_01()


if __name__ == "__main__" and hasattr(main, "__file__"):
    make_plots()
