E2059
=====

.. note::
  The figure(s) given herein exist without technical context and for the sole
  purpose of communicating how they are generated.  If one wishes to use the
  figure(s) (and generating processes) herein, he or she must consult the
  associated technical standard, described next.

This chapter describes how to produce the figures for ASTM E10.05 Standard Test
Method 2059.  The recommended citation for this standard is:

* ASTM E2059-15e1, Standard Practice for Application and Analysis of Nuclear
  Research Emulsions for Fast Neutron Dosimetry, ASTM International, West
  Conshohocken, PA, 2015, www.astm.org

