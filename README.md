# ASTM Data Collator

This collection of scripts, data-access utilities, and accompanying
documentation is used to

1. preserve the knowledge of how to retrieve and post-process the data that
   underpins the figures used in E10 committee standards and
2. prepare the resulting figures using a consistent look and feel.

In addition, some source files for figures that fall under E10's purview are
contained herein to ensure that they are available for members to work with on
an ongoing basis.

Standards with coverage currently include:

* E263, Standard Test Method for Measuring Fast-Neutron Reaction Rates
  by Radioactivation of Iron
* E264, Standard Test Method for Measuring Fast-Neutron Reaction Rates by
  Radioactivation of Nickel
* E266, Standard Test Method for Measuring Fast-Neutron Reaction Rates by
  Radioactivation of Aluminum
* E481, Standard Test Method for Measuring Neutron Fluence Rates by
  Radioactivation of Cobalt and Silver
* E523, Standard Test Method for Measuring Fast-Neutron Reaction Rates
  by Radioactivation of Copper
* E666, Standard Practice for Calculating Absorbed Dose from Gamma or X
  Radiation
* E693, Standard Practice for Characterizing Neutron Exposures in Iron and Low
  Alloy Steels in Terms of Displacements Per Atom (DPA)
* E704, Standard Test Method for Measuring Reaction Rates by Radioactivation of
  Uranium-238
* E705, Standard Test Method for Measuring Reaction Rates by Radioactivation of
  Neptunium-237
* E844, Standard Guide for Sensor Set Design and Irradiation for Reactor
  Surveillance
* E1005, Standard Test Method for Application and Analysis of Radiometric
  Monitors for Reactor Vessel Surveillance
* E1297, Standard Test Method for Measuring Fast-Neutron Reaction Rates by
  Radioactivation of Niobium

# How To Use These Tools

To perform a wholesale generation of all supported figures, simply executing
`python3 make_all_figures.py` should be sufficient.  Within `make_all_figures.py`,
individual standards can be enabled or disabled.

Likewise, within the `scripts_generate_figures` subdirectory individual files
used to encapsulate each E10 standard can be modified to enable/disable
production of individual figures.  Further, fine-grained control can be
exercised over the appearance of each figure via these individual controller
files.

## Documentation

HTML documentation is available via the GitLab Pages site
[here](https://astm_e10_data_collator.gitlab.io/ASTM_E10_Produce_Figures/).

This documentation can be generated via Sphinx within the `doc`
directory.  The easiest way to generate this documentation is with

```
cd doc
make html
```

or

```
cd doc
make latexpdf
```

if your system has `pdflatex` available.

## Requirements

These utilities (and the associated documentation) have successfully been
exercised using Python 3 (v. 3.6.5) provided via Homebrew on macOS 10.13.5.
Additional packages needed include:

* numpy
* cycler
* matplotlib
* sphinx (for documentation)
* pdflatex (for documentation)

# How to Use, and Contribute to, this Repository

Using a decentralized git-based workflow is unfamiliar to many E10 members.
Because of this, some background on git is provided and a basic workflow is
described in this section.  However, neither of these descriptions are
exhaustive and users are encourage to consult git and GitLab's documentation, as
appropriate.

## Background on git and GitLab

The git software, upon which GitLab runs, is open-source decentralized
version-control software.  The popularity of this approach to version control
has grown immensely since it was introduced and is arguably the most-popular
approach currently.  The git software is most commonly used to permit
distributed teams to develop software in a collaborative setting.

However, git is strictly version control software.  Because other tools are
useful when developing software for tasks such as documentation, testing, etc.,
a variety of interfaces that sit above git have been developed.  One such
interface is GitLab.  GitLab was chosen for this work because it includes
several capabilities without additional configuration that make generating
figures (such as those needed in ASTM standards) and accompanying documentation
relatively straightforward.

## Working with GitLab

GitLab is used to store the data to generate ASTM figures using the also-stored
scripts.  GitLab also includes functionality to perform continuous-integration
(CI) testing, which is intended to test software each time a change is made to it.
For the purpose of this work, CI is used to regenerate all figures and the
accompanying documentation each time a change is made.

Changes are proposed through the "merge request" process.  The merge request
process permits a variety of users to base their work on a centralized
repository while keeping all the changes segregated.  When a given change is
ready to be incorporated into the centralized repository, the user issues a
"merge request" back to centralized repository such that the centralized
repository's maintainer is "requested" to "merge" the user's contribution.

The steps to perform a merge request from the point that the user has just
created an account are:

1. The user forks the `ASTM_E10_Produce_Figures` repository to his or her
   user space.  This creates a copy that can be freely modified without
   affecting the centralized repository.
2. The user creates a branch on their new repository.  In that branch, all
   changes for a particular task are made through one or more commits to the
   user's repository.
3. When all changes/commits have been made and pushed to the
   user's forked repository, a merge request is issued to merge the user's
   changes into the `master` branch for the centralized
   `ASTM_E10_Produce_Figures` repository.

## Contributed Code Style

The Python scripts in this repository are formatted using the
[`black`](https://github.com/psf/black) utility.  The GitLab CI pipeline checks
for valid `black` formatting during a static-analysis stage, so all work must
abide this styling.  Thus, one must apply `black` manually to all committed
code, which can be assisted by [pre-commit
hooks](https://ljvmiranda921.github.io/notebook/2018/06/21/precommits-using-black-and-flake8/)
to verify that has been done.

# Contact

This collection of capabilities was assembled to support [ASTM committee
E10](https://www.astm.org/COMMITTEE/E10.htm), which focuses on
Nuclear Technology and Applications.

Questions should be directed to the chairperson of that committee.
